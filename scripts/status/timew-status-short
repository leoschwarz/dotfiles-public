#!/usr/bin/env python3
# apt python3-dateutil or pip python-dateutil

import datetime
import dateutil.parser
import json
import os
import subprocess

# toggle if i3blocks button was pressed
if os.environ.get("BLOCK_BUTTON"):
    active = subprocess.run("timew", stdout=subprocess.DEVNULL).returncode == 0
    if active:
        subprocess.run(["timew", "stop"], stdout=subprocess.DEVNULL)
    else:
        subprocess.run(["timew", "start"], stdout=subprocess.DEVNULL)

entries = json.loads(subprocess.run(
    ["timew", "export", "today"],
    stdout=subprocess.PIPE,
    check=True).stdout.decode("utf8"))

if not entries:
    # no logged task for today
    print("00:00:00  (00:00:00)")
else:
    # Convert all times to utc time points.
    for entry in entries:
        if "end" in entry:
            entry["end"] = dateutil.parser.parse(entry["end"]).replace(tzinfo=None)
        else:
            entry["end"] = None
        if "start" in entry:
            entry["start"] = dateutil.parser.parse(entry["start"]).replace(tzinfo=None)

    # Compute total duration of work for this day.
    total = datetime.timedelta(0)
    for entry in entries:
        start = entry["start"]
        end = entry["end"]
        if end is None:
            end = datetime.datetime.utcnow().replace(tzinfo=None)
        delta = end - start
        total += delta

    if entries[-1]["end"] is None:
        # The last entry is still ongoing, so indicate that.
        active = "+"
        current = delta
    else:
        # The last entry is finished, display the time that passed since then.
        active = "/"

        now = datetime.datetime.utcnow().replace(tzinfo=None)
        current = now - entries[-1]["end"]

    # Format the timedeltas as HH:MM:SS
    current = str(datetime.timedelta(seconds=current.seconds)).rjust(8, "0")
    total = str(datetime.timedelta(seconds=total.seconds)).rjust(8, "0")

    print("{}{} ({})".format(current, active, total))
