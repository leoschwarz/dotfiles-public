#!/bin/bash

iwconfig_output="$(/sbin/iwconfig wlp3s0)"

essid=$(echo $iwconfig_output | grep -oP '(?<=ESSID:")[^"]*')

# e.g. 51/70
quality_frac=$(echo $iwconfig_output | grep -oP '(?<=Link Quality=)[0-9/]*')
echo $quality_frac
quality=$(awk 'BEGIN{printf "%2.f%", frac}' frac="$quality_frac")
echo $quality

# wrong bitrate on my Debian laptop, see: https://bugs.launchpad.net/ubuntu/+source/wireless-tools/+bug/1492180
#bitrate=$(/sbin/iwconfig wlp3s0 | grep -oP '(?<=Bit Rate=)[0-9]* .?b/s')
bitrate=$(/sbin/iw dev wlp3s0 link | grep -oP '(?<=tx bitrate: ).*/s')

if [ "$essid" ] && [ "$bitrate" ]; then
    echo "$essid $bitrate"
else
    echo "not connected"
fi

