# This file includes the basic functions that will be needed by the
# rest of the ZSH configuration files.

# Like `source FILE` but only executed if the file exists and doesn't
# cause any issues/warnings if the file doesn't exist.
source_if_exists () {
	if [ -f "$1" ]; then
		source "$1"
	fi
}

## Shortcut to source something inside zsh_extensions.
#source_zsh_extension () {
#	source "$HOME/.zsh_extensions/$1"
#}

# Appends a directory to the end of PATH if the dir exists.
# If this is successful it will return 0, otherwise return code is 1.
# Usage:
#   append_path /example/directory
append_path () {
	if [ -d "$1" ]; then
		export PATH="$PATH:$1"
		return 0
	else
		return 1
	fi
}

# See: append_path
# This behaves exactly the same except that the path is appended at the
# beginning of $PATH.
append_path_beginning() {
	if [ -d "$1" ]; then
		export PATH="$1":$PATH
		return 0
	else
		return 1
	fi
}

# Checks if a binary is found for a specified command name.
command_exists() {
    command -v $1 >/dev/null 2>&1
    return $?
}

