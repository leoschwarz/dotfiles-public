# Configuration for the zsh prompt.

# Load required zsh extensions.
autoload -U promptinit compinit && compinit && promptinit
autoload -U colors && colors
source "$DOTFILES_PUBLIC/modules/zsh/extensions/git-prompt.zsh"
source "$DOTFILES_PUBLIC/modules/zsh/extensions/zsh-history-substring-search/zsh-history-substring-search.zsh"

# Syntax highlighting (Debian package zsh-syntax-highlighting).
source_if_exists /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Case insensitive tab completion.
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'

# Prompt based on walters and reporting git-prompt's status.
export GIT_PS1_SHOWDIRTYSTATE=1
export GIT_PS1_SHOWSTASHSTATE=1
export GIT_PS1_SHOWUNTRACKEDFILES=1
setopt PROMPT_SUBST
#export PROMPT='%B%(?..[%?] )%b%n@%U%m%u$(__git_ps1)> '
export PROMPT='%B%(?..[%?] )%b%n@%U%m%u> '
export RPROMPT="%{$fg[green]%}%~"

# Make username red if superuser.
if [[ $UID == 0 || $EUID == 0 ]]; then
    export PROMPT='%B%(?..[%?] )%b%{$fg[red]%}%n%{$reset_color%}@%U%m%u$(__git_ps1)> '
fi

# History storage configuration.
setopt HIST_IGNORE_DUPS
HISTFILE=$HOME/.zhistory
HISTSIZE=100000
SAVEHIST=100000

# History prompt configuration.
# bind UP and DOWN arrow keys
zmodload zsh/terminfo
bindkey "$terminfo[kcuu1]" history-substring-search-up
bindkey "$terminfo[kcud1]" history-substring-search-down

# bind UP and DOWN arrow keys (compatibility fallback
# for Ubuntu 12.04, Fedora 21, and MacOSX 10.9 users)
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

# bind k and j for VI mode
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down

# Remove problematic PgUp, PgDn behavior.
function do-nothing { }
zle -N do-nothing
bindkey '^[[5~' do-nothing
bindkey '^[[6~' do-nothing

