# This file contains a set of useful shell functions that I like to use
# for my own conveniency.

# Hyperdownload.
# Never again downloading files will be slow again.
# Usage:
#   hyperdownload http://example.com/bigfile.zip [-o DESTINATION]
alias hyperdownload='aria2c -s 32 -x 16 -k 10M --file-allocation=none '

# Multi-MV (mmv).
# Move multiple files according to a regex directly from the shell
# to save your precious time from this tedious task.
# Usage:
#   zmv *.dat *.dat_old
# Source:
#   http://www.mfasold.net/blog/2008/11/moving-or-renaming-multiple-files/
autoload -U zmv
alias mmv='noglob zmv -W'

# Trash files command. (Moves files to KDE trash folder.)
trash() {
    # Make sure that KDE IO is actually available.
    if command_exists kioclient; then
        if [ "$#" -eq 0 ]; then
            echo "Usage: trash path1 [path2 [path3 [...]]]"
        else
            for entry in "$@"
            do
                kioclient move $entry trash:/
            done
        fi
    else
        echo 'Error: kioclient not found. Is KDE installed?'
    fi
}

# Mac-like terminal paste board access using xsel.
alias pbcopy="xsel --clipboard --input"
alias pbpaste="xsel --clipboard --output"

set-wallpaper() {
    if [ "$#" -eq 1 ]
    then
        rm ~/.wallpaper.jpg
        ln -s $1 ~/.wallpaper.jpg
        feh --bg-fill ~/.wallpaper.jpg
    else
        echo "Usage: set-wallpaper FILEPATH"
    fi
}

# Source: http://tuxdiary.com/2015/04/07/compress-pdf-files-linux/
pdf-minify() {
    if [ "$#" -eq 2 ]
    then
        gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$2 $1
    elif [ "$#" -eq 1 ]
    then
        # Append .min.pdf to the filename without the pdf extension.
        local src=`basename $1 .pdf`
        local dest=$src.min.pdf
        pdf-minify $1 $dest
    else
        echo "Lossful compression of a PDF file using Ghostscript.\n"
        echo "Usage: pdf-minify SOURCEPDF [DESTPDF]"
        return 1
    fi
}

#alias youtube-dl-plus='youtube-dl -x -k --audio-format best --audio-quality 0 --external-downloader aria2c --external-downloader-args "-s 4 -x 4 -k 1M --file-allocation=none"'
alias youtube-dl-plus-nokeep='youtube-dl -x --write-description --write-info-json -f best --embed-thumbnail'
alias youtube-dl-plus-keep='youtube-dl -x -k --write-description --write-info-json -f best --embed-thumbnail'

# src: https://superuser.com/a/751809
#      https://superuser.com/a/1260695
alias makec="unbuffer make |& less -r"

# because I always forget it
ssh-forward-to-remote() {
	if [ "$#" -eq 3 ]
	then
		ssh -L ${1}:localhost:${3} $2
	else
		echo "Usage: $0 LOCAL_PORT HOST REMOTE_PORT"
		return 1
	fi
}

