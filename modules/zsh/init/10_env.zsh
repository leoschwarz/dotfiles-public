# All configuration that don't have anything to do with the path do belong here.

# Python build configuration.
export PYTHON_CONFIGURE_OPTS="--enable-shared"

# Linux specific configuration.
if [[ `uname` == "Linux" ]]; then
    alias ls="ls --color "

    # CCache config.
    export CCACHE_COMPRESS=1

    # Quick alias because I am used to the open command on OS X.
    alias open="xdg-open"
fi

# Use exa if it is installed instead of ls.
if [[ -n `command -v exa` ]];
then
    alias ls="exa "
fi

# If jupyter is installed alias ipython to it,
# on Debian packages `jupyter-core`, `jupyter-console` and `python3-ipykernel` are needed.
# TODO: enable in the future, right now `jupyter console` takes almost twice as long to start as calling ipython3 directly.
#if [[ -n `command -v jupyter` ]]
#then
#    alias ipython="jupyter console"
#    alias ipython3="jupyter console"
#fi

# Editor configuration.
# Rule: use if present (in this order): nvim, vim, nano.
if [[ -n `command -v nvim` ]]
then
    export VISUAL="nvim"
    export EDITOR="nvim"
    alias vim="nvim"
elif [[ -n `command -v vim` ]]
then
    export VISUAL="vim"
    export EDITOR="vim"
else
    export VISUAL="nano"
    export EDITOR="nano"
fi

