import argparse
import subprocess
import glob
import os


def get_modules(args):
    if args.MODULE:
        return [args.MODULE]
    else:
        module_dir = os.path.abspath(os.path.dirname(__file__))
        return [os.path.basename(os.path.dirname(d)) for d in glob.glob(f"{module_dir}/*/")]


def run(args):
    if args.action == "add":
        run_add(get_modules(args))
    elif args.action == "remove":
        run_remove(get_modules(args))
    elif args.action == "sync":
        run_remove(get_modules(args))
        run_add(get_modules(args))


def run_add(modules):
    for module in modules:
        subprocess.run(["stow",
                # Install to user home
                "-t", os.path.expanduser("~"),
                module])


def run_remove(modules):
    for module in modules:
        subprocess.run(["stow",
                "--delete",
                "-t", os.path.expanduser("~"),
                module])


def main():
    parser = argparse.ArgumentParser(description="Manage installed modules.")
    sub = parser.add_subparsers(dest="action", required=True)

    p_add = sub.add_parser("add")
    g = p_add.add_mutually_exclusive_group(required=True)
    g.add_argument("MODULE", nargs="?")
    g.add_argument("--all", action="store_true")

    p_remove = sub.add_parser("remove")
    g = p_remove.add_mutually_exclusive_group(required=True)
    g.add_argument("MODULE", nargs="?")
    g.add_argument("--all", action="store_true")

    p_sync = sub.add_parser("sync")
    g = p_sync.add_mutually_exclusive_group(required=True)
    g.add_argument("MODULE", nargs="?")
    g.add_argument("--all", action="store_true")

    args = parser.parse_args()
    run(args)

if __name__ == "__main__":
    main()
